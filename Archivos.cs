﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Automata
{
    class Archivos
    {
        public int Id { get; set; }
        public string NombreArchivo { get; set; }
        public int Estatus { get; set; }
        public double Num_Reg { get; set; }
        public DateTime Fecha_Insercion { get; set; }
        public string UrlArchivo { get; set; }
        public DateTime Fecha_Transaccion { get; set; }
        public List<Cabecera> Registros { get; set; }

        public string Error { get; set; }
    }
}
