﻿using System;
using System.Collections.Generic;

namespace Automata
{
    class Cabecera
    {
        public int Registro { get; set; }
        public int Clave_pais { get; set; }
        public string Sucursal_Apertura { get; set; }
        public int Cuenta { get; set; }
        public DateTime Fecha_Inicial { get; set; }
        public DateTime Fecha_Final { get; set; }
        public  int Id_Saldo { get; set; }
        public int Saldo_Operativo_Ini { get; set; }
        public int Id_Moneda { get; set; }
        public string Dig_Cuenta_Clabe { get; set; }
        public string Titular { get; set; }
        public string Plaza_Cuenta_clabe { get; set; }
        public string Filler { get; set; }
        public DateTime Fecha_Insercion { get; set; }
        public int Id_Archivo { get; set; }
        public List<Movimiento> Movimientos { get; set; }
        public List<Totales> Totales { get; set; }
    }
}
