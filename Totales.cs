﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Automata
{
    class Totales
    {

        public int Id_Reg { get; set; }
        public int Id_Cabecera { get; set; }
        public string Clave_Pais { get; set; }
        public int Id_Saldo { get; set; }
        public string Saldo_Inicial { get; set; }
        public string Saldo_Final { get; set; }
        public int Id_Pais { get; set; }
        public double Sucursal_Apertura { get; set; }
        public double Cuenta { get; set; }
        public double No_Cargos { get; set; }
        public double Importe_Cargos { get; set; }
        public double No_Bonos { get; set; }
        public double Importe_Bonos { get; set; }
        public int Id_Saldo2 { get; set; }
        public double Saldo_Operativo_Final { get; set; }
        public int Id_Moneda { get; set; }
        public DateTime Fecha_Insercion { get; set; }
        public int Id_Archivo { get; set; }
    }
}
