﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automata
{
    class Program
    {
        public static List<Archivos> LstArchLocales = new List<Archivos>();
        public static List<string> LstArchBD = new List<string>();
        public static List<Archivos> LstArchFaltantes = new List<Archivos>();

        //public static string UrlArchivosLocales = @"C:\Users\Lopezval\Documents\Archivos";
        //public static string UrlArchivosLocales = @"\\192.168.10.248\bbva";
        //public static string UrlArchivoLog = @"C:\Users\Lopezval\Documents\Archivos\LogAutomatizacion.txt";

        public static string UrlArchivosLocales = "";
        public static string UrlArchivoLog = "";
        public static string UrlBD = "";


        public static void GuardarLog(string Tipo, string Mensaje)
        {
            //if (File.Exists(UrlArchivoLog))
            //{
            //    using (StreamWriter sw = File.AppendText(UrlArchivoLog))
            //    {
            //        sw.WriteLine(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "-" + Tipo + "-" + Mensaje);
            //    }
            //}
            //else
            //{
            //    using (FileStream fs = File.Create(UrlArchivoLog))
            //    {
            //        byte[] info = new UTF8Encoding(true).GetBytes(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "-" + Tipo + "-" + Mensaje);
            //        fs.Write(info, 0, info.Length);
            //    }
            //}
            try
            {
                using (SqlConnection connection = new SqlConnection(UrlBD))
                {
                    connection.Open();
                    using (var command = connection.CreateCommand())
                    {
                        string QueryInsert = String.Format("INSERT INTO [dbo].[Log_Eventos] " + "([Fecha_Evento],[Mensaje])VALUES ('{0}','{1}')",DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),Mensaje);
                        SqlCommand cmd = new SqlCommand(QueryInsert, connection);
                        cmd.ExecuteNonQuery();
                    }
                } 
            }
            catch (Exception e)
            {

            }
        }
        static void Main(string[] args)
        {
            var appSettings = ConfigurationManager.AppSettings;
            UrlArchivoLog = appSettings["UrlArchivoLOG"].ToString();
            UrlArchivosLocales = appSettings["UrlArchivosBBVA"].ToString();
            //UrlBD = "server=MX5CD9271VD3P52\\SQLEXPRESS; database = BDAutomatizacion; User ID = sa; Password =sa123; Application Name =Automata";
            //UrlBD = appSettings["ConexionBDDev"].ToString();
            UrlBD = appSettings["ConexionBD"].ToString();
            
            EjecutarConsultas();
        }
        public static void EjecutarConsultas()
        {
            GuardarLog("Informacion", "Iniciar Lectura de informacion");
            if (ConexionBD())
            {
                ObtenerLstArchivosLocales();
                ObtenerArchivosFaltantes();
                if (LstArchFaltantes.Count > 0)
                {
                    leerInformacionArchivos();
                    GuardarInformacionBD();
                }
                else
                {
                    GuardarLog("Informacion", "Sin información para actualizar");
                }

            }
            else
            {
                //GuardarLog("Error de Conexion", "No se pudo Conectar a la base de datos");
            }
        }

        public static bool ConexionBD()
        {
            using (SqlConnection connection = new SqlConnection(UrlBD))
            {
                try
                {
                    connection.Open();
                    connection.Close();
                    return true;
                }
                catch (Exception exception)
                {
                    //GuardarLog("Error de Conexion", exception.Message);
                    return false;
                }
            }
        }

        public static void ObtenerLstArchivosLocales()
        {
            LstArchLocales = new List<Archivos>();
            DirectoryInfo di = new DirectoryInfo(UrlArchivosLocales);
            int Conexion = -1;
            try
            {
                Conexion = di.GetFiles("*", SearchOption.AllDirectories).Count();
            }
            catch (Exception e)
            {
                GuardarLog("Error de Conexion", "Sin Acceso al directorio: " + e.Message);
            }

            if (Conexion != -1)
            {
                foreach (var fi in di.GetFiles("*", SearchOption.AllDirectories))
                {
                    try
                    {
                        string url = fi.DirectoryName.Substring(fi.DirectoryName.Length - 6, 6);
                        string pattern = "ddMMyy";
                        DateTime f_Transaccion;
                        DateTime.TryParseExact(url, pattern, null, DateTimeStyles.None, out f_Transaccion);
                        LstArchLocales.Add(new Archivos() { NombreArchivo = fi.Name, UrlArchivo = fi.FullName, Estatus = 1, Num_Reg = 0, Fecha_Transaccion = f_Transaccion });
                    }
                    catch (Exception e)
                    {
                        GuardarLog("Error de Conexion", "Sin Acceso al archivo: " + e.Message);
                    }
                }
            }

        }

        public static bool ObtenerArchivosFaltantes()
        {
            LstArchBD = new List<string>();
            LstArchFaltantes = new List<Archivos>();
            using (SqlConnection connection = new SqlConnection(UrlBD))
            {
                try
                {
                    connection.Open();
                    using (var command = connection.CreateCommand())
                    {
                        string query = @"SELECT [Nombre_Archivo] FROM [dbo].[Reg_Archivos]";
                        command.CommandText = query;
                        DbDataReader reader = command.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                LstArchBD.Add(reader.GetString(0).Trim());
                            }
                        }
                        reader.Dispose();
                    }

                    var filtered = (LstArchLocales.Where(i => !LstArchBD.Contains(i.NombreArchivo))).ToList();

                    foreach (var item in filtered)
                    {
                        LstArchFaltantes.Add(item);
                    }

                    connection.Close();
                    return true;
                }
                catch (Exception exception)
                {
                    //GuardarLog("Error de Base de datos", exception.Message);
                    return false;
                }
            }
        }

        public static void leerInformacionArchivos()
        {
            string pattern = "yyMMdd";
            foreach (var itemSup in LstArchFaltantes)
            {
                try
                {
                    itemSup.Registros = new List<Cabecera>();
                    string[] lines = System.IO.File.ReadAllLines(itemSup.UrlArchivo);
                    List<Cabecera> ListCabeceras = new List<Cabecera>();
                    int reg = 0;
                    for (int i = 0; i < lines.Count(); i++)
                    {
                        string item = lines[i];
                        if (item.Length == 80 && item.Substring(0, 2) == "11")
                        {
                            Cabecera itemCabecera = new Cabecera();
                            itemCabecera.Clave_pais = Convert.ToInt32(item.Substring(2, 4).ToString());
                            itemCabecera.Sucursal_Apertura = item.Substring(6, 4).ToString();
                            itemCabecera.Cuenta = Convert.ToInt32(item.Substring(10, 10).ToString());

                            DateTime parsedDate;
                            DateTime parsedDate2;
                            DateTime.TryParseExact(item.Substring(20, 6).ToString(), pattern, null, DateTimeStyles.None, out parsedDate);
                            DateTime.TryParseExact(item.Substring(26, 6).ToString(), pattern, null, DateTimeStyles.None, out parsedDate2);
                            itemCabecera.Fecha_Inicial = parsedDate;
                            itemCabecera.Fecha_Final = parsedDate2;
                            itemCabecera.Id_Saldo = Convert.ToInt32(item.Substring(32, 1).ToString());
                            itemCabecera.Saldo_Operativo_Ini = Convert.ToInt32(item.Substring(33, 14).ToString())/100;
                            itemCabecera.Id_Moneda = ObtenerIdMoneda(item.Substring(47, 3).ToString());
                            itemCabecera.Dig_Cuenta_Clabe = item.Substring(50, 1).ToString();
                            itemCabecera.Titular = item.Substring(51, 23);
                            itemCabecera.Plaza_Cuenta_clabe = item.Substring(74, 3);
                            itemCabecera.Filler = item.Substring(77, 3);
                            itemCabecera.Fecha_Insercion = DateTime.Now;
                            itemCabecera.Movimientos = new List<Movimiento>();
                            itemCabecera.Totales = new List<Totales>();
                            ListCabeceras.Add(itemCabecera);
                            reg++;
                        }
                        if (item.Length == 80 && item.Substring(0, 2) == "22")
                        {

                            Movimiento itemMov = new Movimiento();
                            itemMov.Clave_Pais = Convert.ToInt32(item.Substring(2, 4).ToString());
                            itemMov.Sucursal = item.Substring(6, 4).ToString();
                            DateTime fOperacion;
                            DateTime fValor;
                            DateTime.TryParseExact(item.Substring(10, 6).ToString(), pattern, null, DateTimeStyles.None, out fOperacion);
                            DateTime.TryParseExact(item.Substring(16, 6).ToString(), pattern, null, DateTimeStyles.None, out fValor);
                            itemMov.Fecha_Operacion = fOperacion;
                            itemMov.Fecha_Valor = fValor;
                            itemMov.Filler = item.Substring(22, 2).ToString();
                            itemMov.Codigo_Leyenda = item.Substring(24, 3).ToString();
                            itemMov.Id_Operacion = Convert.ToInt32(item.Substring(27, 1).ToString());
                            itemMov.Importe = Convert.ToDouble(item.Substring(28, 14).ToString())/100;
                            itemMov.Dato = item.Substring(42, 10).ToString();
                            itemMov.Concepto = item.Substring(52, 28).ToString();

                            if ((i + 1) < lines.Count())
                            {
                                if (lines[(i + 1)].Length == 80 && lines[(i + 1)].Substring(0, 2) == "23")
                                    i = i + 1;
                                itemMov.Codigo_Dato = Convert.ToInt32(lines[i].Substring(2, 2).ToString());
                                itemMov.Referencia_Ampliada = lines[i].Substring(4, 38).ToString();
                                itemMov.Referencia = lines[i].Substring(42, 38).ToString();
                                itemMov.Fecha_Insercion = DateTime.Now;
                            }
                            ListCabeceras[ListCabeceras.Count - 1].Movimientos.Add(itemMov);
                            reg++;
                        }
                        if (item.Length == 77 && item.Substring(0, 2) == "32")
                        {

                            Totales itemTot = new Totales();
                            itemTot.Clave_Pais = item.Substring(2, 3).ToString();
                            itemTot.Id_Saldo = Convert.ToInt32(item.Substring(5, 2).ToString());
                            itemTot.Saldo_Final = item.Substring(7, 35).ToString();
                            itemTot.Saldo_Inicial = item.Substring(42, 35).ToString();
                            itemTot.Fecha_Insercion = DateTime.Now;
                            if ((i + 1) < lines.Count())
                            {
                                if (lines[(i + 1)].Length == 76 && lines[(i + 1)].Substring(0, 2) == "33")
                                    i = i + 1;
                                itemTot.Id_Pais = Convert.ToInt32(lines[i].Substring(2, 4).ToString());
                                itemTot.Sucursal_Apertura = Convert.ToDouble(lines[i].Substring(6, 4).ToString());
                                itemTot.Cuenta = Convert.ToDouble(lines[i].Substring(10, 10).ToString());
                                itemTot.No_Cargos = Convert.ToDouble(lines[i].Substring(20, 5).ToString());
                                itemTot.Importe_Cargos = Convert.ToDouble(lines[i].Substring(25, 14).ToString())/100;
                                itemTot.No_Bonos = Convert.ToDouble(lines[i].Substring(39, 5).ToString());
                                itemTot.Importe_Bonos = Convert.ToDouble(lines[i].Substring(44, 14).ToString())/100;
                                itemTot.Id_Saldo2 = Convert.ToInt32(lines[i].Substring(58, 1).ToString());
                                itemTot.Saldo_Operativo_Final = Convert.ToDouble(lines[i].Substring(59, 14).ToString())/100;
                                itemTot.Id_Moneda = ObtenerIdMoneda(lines[i].Substring(73, 3).ToString());
                            }
                            ListCabeceras[ListCabeceras.Count - 1].Totales.Add(itemTot);
                            reg++;
                        }
                    }
                    itemSup.Registros = ListCabeceras;
                    itemSup.Num_Reg = reg;
                }
                catch (Exception e)
                {
                    GuardarLog("Error de lectura", " " + itemSup.NombreArchivo + "-" + e.Message);
                }

            }
        }

        private static void GuardarInformacionBD()
        {

            using (SqlConnection connection = new SqlConnection(UrlBD))
            {
                try
                {
                    connection.Open();

                    using (var command = connection.CreateCommand())
                    {
                        var itemReg = 0;
                        try
                        {
                            foreach (var item in LstArchFaltantes)
                            {
                                string queryt = String.Format("INSERT INTO[dbo].[Reg_Archivos] " + "([Nombre_Archivo],[Estatus],[Registros],[Fecha_Insercion],[Url],[Fecha_Transaccion],[Error])" +
                                "VALUES ('{0}',  {1}, {2}, '{3}','{4}','{5}','') SELECT SCOPE_IDENTITY()", item.NombreArchivo, 1, item.Num_Reg, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), item.UrlArchivo, item.Fecha_Transaccion.ToString("dd/MM/yyyy"));
                                SqlCommand cmd = new SqlCommand(queryt, connection);
                                item.Id = Convert.ToInt32(cmd.ExecuteScalar());
                                itemReg = item.Id;
                                foreach (var cabecera in item.Registros)
                                {
                                    cabecera.Id_Archivo = item.Id;

                                    string queryCab = String.Format("INSERT INTO[dbo].[Reg_Cabecera] ([Clave_Pais],[Sucursal_Apertura] ,[Cuenta] ,[Fecha_Inicial],[Fecha_Final],[Id_TipoSaldo] " +
                                                    " ,[Saldo_Operativo_inicial],[Id_Moneda] ,[Digito_Cuenta_Clabe],[Titular],[Plaza_Cuenta_Clabe],[Filler],[Fecha_Insercion],[Id_Archivo]) VALUES " +
                                                    "({0},{1},{2},'{3}','{4}',{5},{6},{7},'{8}','{9}','{10}','{11}','{12}',{13}) SELECT SCOPE_IDENTITY()", cabecera.Clave_pais, cabecera.Sucursal_Apertura, cabecera.Cuenta, cabecera.Fecha_Inicial.ToString("dd/MM/yyyy HH:mm:ss"), cabecera.Fecha_Final.ToString("dd/MM/yyyy HH:mm:ss"),
                                                    cabecera.Id_Saldo, cabecera.Saldo_Operativo_Ini, cabecera.Id_Moneda, cabecera.Dig_Cuenta_Clabe, cabecera.Titular, cabecera.Plaza_Cuenta_clabe, cabecera.Filler, cabecera.Fecha_Insercion.ToString("dd/MM/yyyy HH:mm:ss"), cabecera.Id_Archivo);
                                    SqlCommand cmd2 = new SqlCommand(queryCab, connection);
                                    cabecera.Registro = Convert.ToInt32(cmd2.ExecuteScalar());
                                    foreach (var mov in cabecera.Movimientos)
                                    {
                                        mov.Id_Archivo = item.Id;
                                        mov.Id_Red_Cabecera = cabecera.Registro;
                                        string queryMov = String.Format(" INSERT INTO [dbo].[Reg_Movimientos] ([Id_Reg_Cabecera], [Clave_Pais], [Sucursal], [Fecha_Operacion] , [Fecha_Valor], [Filler], [Codigo_Leyenda], [Id_Operacion]," +
                                        " [Importe], [Dato], [Concepto], [Codigo_Dato], [Referencia_Ampliada], [Referencia], [Fecha_Insercion], [Id_Archivo]) VALUES " +
                                                        "({0},{1},'{2}','{3}','{4}','{5}','{6}',{7},{8},'{9}','{10}',{11},'{12}','{13}','{14}',{15})",
                                                        mov.Id_Red_Cabecera, mov.Clave_Pais, mov.Sucursal, mov.Fecha_Operacion.ToString("dd/MM/yyyy HH:mm:ss"), mov.Fecha_Valor.ToString("dd/MM/yyyy HH:mm:ss"), mov.Filler, mov.Codigo_Leyenda, mov.Id_Operacion,
                                                        mov.Importe, mov.Dato, mov.Concepto, mov.Codigo_Dato, mov.Referencia_Ampliada, mov.Referencia, mov.Fecha_Insercion.ToString("dd/MM/yyyy HH:mm:ss"), mov.Id_Archivo);
                                        SqlCommand cmd3 = new SqlCommand(queryMov, connection);
                                        cmd3.ExecuteNonQuery();
                                    }
                                    foreach (var tot in cabecera.Totales)
                                    {
                                        tot.Id_Archivo = item.Id;
                                        tot.Id_Cabecera = cabecera.Registro;
                                        string queryTot = String.Format(" INSERT INTO [dbo].[Reg_Totales] ([Id_Reg_Cabecera],[Clave_Pais],[Id_Saldo],[Saldo_valor_Inicial],[Saldo_valor_final],[Id_Pais],[Sucursal_Apertura] ,[Cuenta],[No_Cargos]," +
                                        " [Importe_Cargos],[No_Bonos] ,[Importe_Bonos],[Id_Saldo2],[Saldo_Operativo_final],[Id_Moneda] ,[Fecha_Insercion],[Id_Archivo]) VALUES " +
                                                        "({0},'{1}',{2},'{3}','{4}',{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},'{15}',{16})",
                                                        tot.Id_Cabecera, tot.Clave_Pais, tot.Id_Saldo, tot.Saldo_Inicial, tot.Saldo_Final, tot.Id_Pais, tot.Sucursal_Apertura,
                                                        tot.Cuenta, tot.No_Cargos, tot.Importe_Cargos, tot.No_Bonos, tot.Importe_Bonos, tot.Id_Saldo2, tot.Saldo_Operativo_Final, tot.Id_Moneda,
                                                        tot.Fecha_Insercion.ToString("dd/MM/yyyy HH:mm:ss"), tot.Id_Archivo);
                                        SqlCommand cmd3 = new SqlCommand(queryTot, connection);
                                        cmd3.ExecuteNonQuery();
                                    }

                                }


                                if (item.Num_Reg > 0)
                                {
                                    string queryUpdate = String.Format(" UPDATE [dbo].[Reg_Archivos] SET [Estatus] = 2,[Error]= '' WHERE Id_Archivo = {0}", item.Id);
                                    SqlCommand cmdUpdate = new SqlCommand(queryUpdate, connection);
                                    cmdUpdate.ExecuteNonQuery();
                                }
                                else
                                {
                                    string queryUpdate = String.Format(" UPDATE [dbo].[Reg_Archivos] SET [Estatus]= 4,[Error]= '' WHERE Id_Archivo = {0}", item.Id);
                                    SqlCommand cmdUpdate = new SqlCommand(queryUpdate, connection);
                                    cmdUpdate.ExecuteNonQuery();
                                }

                            }
                        }
                        catch (Exception e)
                        {
                            if (itemReg > 0)
                            {
                                string queryUpdate = String.Format(" UPDATE [dbo].[Reg_Archivos] SET [Estatus] = 3,[Error]= '{1}' WHERE Id_Archivo = {0}", itemReg, e.Message);
                                SqlCommand cmdUpdate = new SqlCommand(queryUpdate, connection);
                                cmdUpdate.ExecuteNonQuery();
                            }
                        }
                    }
                    connection.Close();

                    GuardarLog("Informacion", "Fin de Ejecucion-Se guardaron " + LstArchFaltantes.Count() + " Archivos de texto");
                }
                catch (Exception e)
                {
                    //GuardarLog("Error de Base de datos-", e.Message);
                }
            }
        }

        public static int ObtenerIdMoneda(string Moneda)
        {
            switch (Moneda)
            {
                case "MXP": return 1;
                case "USD": return 2;
                case "EUR": return 3;
                default: return 4;
            }
        }
    }
}
