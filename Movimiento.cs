﻿using System;

namespace Automata
{
    class Movimiento
    {
        public int Id_Reg { get; set; }
        public int Id_Red_Cabecera { get; set; }
        public int Clave_Pais { get; set; }
        public string Sucursal { get; set; }
        public DateTime Fecha_Operacion { get; set; }
        public DateTime Fecha_Valor { get; set; }
        public string Filler { get; set; }
        public string Codigo_Leyenda { get; set; }
        public int Id_Operacion { get; set; }
        public double Importe { get; set; }
        public string Dato { get; set; }
        public string Concepto { get; set; }
        public int Codigo_Dato { get; set; }
        public string Referencia_Ampliada { get; set; }
        public string Referencia { get; set; }
        public DateTime Fecha_Insercion { get; set; }
        public int Id_Archivo { get; set; }
    }
}
